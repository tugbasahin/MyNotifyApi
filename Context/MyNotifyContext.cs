﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MyNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyNotify.Context
{
	public class MyNotifyContext : DbContext
	{
		private readonly IMongoDatabase _database = null;

		public MyNotifyContext(IOptions<Settings> settings)
		{
			var client = new MongoClient(settings.Value.ConnectionString);
			if (client != null)
				_database = client.GetDatabase(settings.Value.Database);
		}

		public IMongoCollection<User> Users
		{
			get
			{
				return _database.GetCollection<User>("User");
			}
		}
		public IMongoCollection<ApplicationUser> ApplicationUsers
        {
            get
            {
				return _database.GetCollection<ApplicationUser>("ApplicationUser");
            }
        }
		public IMongoCollection<Application> Applications
        {
            get
            {
				return _database.GetCollection<Application>("Application");
            }
        }
		public IMongoCollection<ApplicationError> ApplicationErrors
        {
            get
            {
				return _database.GetCollection<ApplicationError>("ApplicationError");
            }
        }
	}
}
