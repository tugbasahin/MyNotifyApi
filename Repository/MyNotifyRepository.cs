﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using MyNotify.Context;
using MyNotify.Intefaces;
using MyNotify.Models;

namespace MyNotify.Repository
{
	public class MyNotifyRepository : IMyNotifyRepository
	{
		private readonly MyNotifyContext _context = null;

		public MyNotifyRepository(IOptions<Settings> settings)
        {
			_context = new MyNotifyContext(settings);
        }

		public async Task AddAppError(ApplicationError item)
		{

            try
            {
				await _context.ApplicationErrors.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }		
		}

		public async Task<IEnumerable<ApplicationError>> GetAllAppError()
		{
			try
            {
				return await _context.ApplicationErrors.Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
		}

		public async Task<ApplicationError> GetAppError(Guid id)
		{
			try
            {
                ObjectId internalId = GetInternalId(id);
				return await _context.ApplicationErrors
					                 .Find(x => x.ApplicationErrorId == id
                                        || x.InternalId == internalId)
                                .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
		}

		private ObjectId GetInternalId(Guid id)
        {
            ObjectId internalId;
			if (!ObjectId.TryParse(id.ToString(), out internalId))
                internalId = ObjectId.Empty;

            return internalId;
        }

		public async Task<bool> RemoveAllAppError()
		{
			try
            {
                DeleteResult actionResult
				= await _context.ApplicationErrors.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
		}

		public async Task<bool> RemoveAppError(Guid id)
		{
			try
            {
                DeleteResult actionResult
				= await _context.ApplicationErrors.DeleteOneAsync(
					Builders<ApplicationError>.Filter.Eq("ApplicationErrorId", id));

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
		}

		public async Task<bool> UpdateAppError(Guid id, string body)
		{
			var filter = Builders<ApplicationError>.Filter.Eq(s => s.ApplicationErrorId, id);
			var update = Builders<ApplicationError>.Update
			                                       .Set(s => s.Title, body)
			                                       .CurrentDate(s => s.Title);

            try
            {
                UpdateResult actionResult
				= await _context.ApplicationErrors.UpdateOneAsync(filter, update);

                return actionResult.IsAcknowledged
                    && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
		}

		public async Task<bool> UpdateNote(Guid id, ApplicationError item)
        {
            try
            {
                ReplaceOneResult actionResult
				= await _context.ApplicationErrors
				                .ReplaceOneAsync(n => n.ApplicationErrorId.Equals(id)
                                            , item
                                            , new UpdateOptions { IsUpsert = true });
                return actionResult.IsAcknowledged
                    && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

		public Task<bool> UpdateAppErrorDocument(Guid id, string body)
		{
			throw new NotImplementedException();
		}
	}
}
