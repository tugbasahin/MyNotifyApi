﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyNotify.Models;

namespace MyNotify.Intefaces
{
    public interface IMyNotifyRepository
    {
		Task<IEnumerable<ApplicationError>> GetAllAppError();
		Task<ApplicationError> GetAppError(Guid id);

		Task AddAppError(ApplicationError item);

		Task<bool> RemoveAppError(Guid id);

		Task<bool> UpdateAppError(Guid id, string body);

		Task<bool> UpdateAppErrorDocument(Guid id, string body);
        
        Task<bool> RemoveAllAppError();
    }
}
