﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MyNotify.Models
{
    [Table("ApplicationError")]
    public class ApplicationError
    {
		[BsonId]
		public ObjectId InternalId { get; set; }

        public Guid ApplicationErrorId { get; set; }
        public string Title { get; set; }
        public string Detail { get; set; }
        public DateTime Date { get; set; }
        public Guid UserId { get; set; }
        public Guid ApplicationId { get; set; }
        public virtual Application Application { get; set; }
        public virtual User User { get; set; }
    }
}
