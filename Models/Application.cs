﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MyNotify.Models
{
    [Table("Application")]
    public class Application
    {
		[BsonId]
		public ObjectId Id { get; set; }

        public Guid ApplicationId { get; set; }
        public string ApplicationName { get; set; }
    }
}
