﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyNotify.Context;
using MyNotify.Intefaces;
using MyNotify.Models;

namespace MyNotify.Controllers
{
	[Produces("application/json")]
    [Route("api/[controller]")]
    public class ApplicationErrorController : Controller
    {
		private readonly IMyNotifyRepository _mynotifyRepository;

		public ApplicationErrorController(IMyNotifyRepository mynotifyRepository)
        {
			_mynotifyRepository = mynotifyRepository;
        }

        //[NoCache]
        [HttpGet]
		public async Task<IEnumerable<ApplicationError>> Get()
        {
			return await _mynotifyRepository.GetAllAppError();
        }

        // GET api/notes/5 - retrieves a specific note using either Id or InternalId (BSonId)
        [HttpGet("{id}")]
		public async Task<ApplicationError> Get(Guid id)
        {
			return await _mynotifyRepository.GetAppError(id) ?? new ApplicationError();
        }

        // POST api/notes - creates a new note
        [HttpPost]
		public void Post([FromBody] ApplicationError applicationError)
        {
			_mynotifyRepository.AddAppError(new ApplicationError
            {
				UserId=applicationError.UserId,
				ApplicationId=applicationError.ApplicationId,
				Title=applicationError.Title,
				Detail=applicationError.Detail,
				Date=DateTime.Now,
            });
        }

        // PUT api/notes/5 - updates a specific note
        [HttpPut("{id}")]
		public void Put(Guid id, [FromBody]string value)
        {
			_mynotifyRepository.UpdateAppErrorDocument(id, value);
        }

        // DELETE api/notes/5 - deletes a specific note
        [HttpDelete("{id}")]
		public void Delete(Guid id)
        {
			_mynotifyRepository.RemoveAppError(id);
        }
    }
}
