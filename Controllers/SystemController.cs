﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyNotify.Context;
using MyNotify.Intefaces;
using MyNotify.Models;

namespace MyNotify.Controllers
{
	[Route("api/[controller]")]
	public class SystemController : Controller
	{

		private readonly IMyNotifyRepository _myNotifyRepository;

		public SystemController(IMyNotifyRepository myNotifyRepository)
		{
			_myNotifyRepository = myNotifyRepository;
		}

		// Call an initialization - api/system/init
		[HttpGet("{setting}")]
		public string Get(string setting)
		{
			if (setting == "mobilizbiz")
			{
				//_myNotifyRepository.RemoveAllAppError();
				_myNotifyRepository.AddAppError(new ApplicationError()
				{
					ApplicationErrorId=Guid.NewGuid(),
					UserId=Guid.NewGuid(),
					Title = "AggreageteException",
					Detail = "Test exception detail"
				});
				return "Done";
			}

			return "Unknown";
		}
	}
}